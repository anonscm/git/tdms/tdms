# This is the user-interface definition of a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com
#

library(shiny)
library(shinyAce)
library(rpivotTable)

library(tdms)

shinyUI(fluidPage(
  theme = "css/style.css", # css

  titlePanel("Mev pivot"),
  sidebarLayout(
    sidebarPanel(
      conditionalPanel(

        condition="input.conditionedPanels==1",
        selectInput("demo_file", label = "Upload a file or select a demo",
                   choices = c("Upload", "Upload CSV flag", "Upload CSV banner",
                               "Paste from Excel",
                               names(tdms.examples()))),
        conditionalPanel(
          condition = "input.demo_file == 'Upload'",
          fileInput("file", label = NA, multiple = FALSE, accept = NULL)),
        conditionalPanel(
          condition = "input.demo_file.substring(0, 10) == 'Upload CSV'",
          fileInput("file1", label = NA, multiple = FALSE,
                    accept = c('text/csv',
                               'text/comma-separated-values',
                               'text/tab-separated-values',
                               'text/plain',
                               '.csv',
                               '.tsv')),
          # checkboxInput('header', 'Header', FALSE),
          # radioButtons('quote', 'Quote', c(None='', 'Double Quote'='"', 'Single Quote'="'"), '"'),
          radioButtons('sep', 'Separator',  c(Comma=',',  Semicolon=';', Tab='\t'),  ',')
        ),
        conditionalPanel(
          condition = "input.demo_file == 'Paste from Excel'",
          textInput('dataName', "Data name for download", value = "impact"),
          radioButtons('decTerminal', 'Decimal separator',
                       c(Comma = ',',  Dot = '.'), '.'),
          radioButtons('sepTerminal', 'Field separator',
                       c(Comma = ',',  Semicolon = ';', Tab = '\t', Space = " "), '\t')
        ),
        conditionalPanel(
          condition = "input.demo_file.substring(0, 5) == 'Demo '",
          downloadButton("downloadDemo", label = "This demo file")
        )
      ),

      conditionalPanel(
        condition="input.conditionedPanels==2",
        textInput('varName', "Name of first row", value = "Id")
      ),

      conditionalPanel(
        condition="input.conditionedPanels==3"
      ),

      # conditionalPanel(
      #   condition="input.conditionedPanels==4",
      #   radioButtons("transpose", label = strong("Transpose"),
      #                choices = list("No" = 0, "Yes" = 1),
      #                selected = 0, inline = TRUE)
      # ),

      conditionalPanel(
        condition="input.conditionedPanels==6",
        tags$label(strong("Output options")),
        checkboxInput("addUniqId", "Add unique id", value = F),
        checkboxInput("dropTotal", "Drop totals", value = T),
        radioButtons("transpose", label = strong("Transpose"),
                     choices = list("No" = 0, "Yes" = 1),
                     selected = 0, inline = TRUE),
        tags$label(strong("Download in format TDMS")),
        downloadButton("downloadData", label = "Download TDMS")
      ),

      conditionalPanel(
        condition="input.conditionedPanels==7",
        helpText(strong("Options")),
        checkboxInput("debugPivot", "Debug rpivotTable", value = F)
        ### textInputRow("nb_after_comma", label = "number after the comma/decimal point", value = "3")
      )
    ),

    mainPanel(
      tabsetPanel(id ="conditionedPanels", type = "pills",
        tabPanel("Input", value=1,
                 conditionalPanel(
                   condition = "input.demo_file == 'Paste from Excel'",
                   br(),
                   # if (require(shinyAce)) {
                   #   aceEditor("textAce",
                   #           value = "", mode = "r", theme = "textmate")
                   # } else {
                   textAreaInput("textArea", label = "",
                                 height = "10em", width = "40em",
                                 placeholder = "Paste your data here")
                   # }
                 ),
                 tableOutput("aceDataframe"),
                 verbatimTextOutput("inputLog")
        ),

        tabPanel("Describe", value=1.5,
                 strong("Input table information"),
                 htmlOutput("input_info"),
                 strong("Table of factors"),
                 tableOutput("input_pData"),
                 strong("Frequencies of pheno (metadata of columns)"),
                 tableOutput("input_phenoFreq"),
                 strong("Table of features"),
                 tableOutput("input_fData"),
                 strong("Frequencies of features (metadata of rows)"),
                 tableOutput("input_featFreq")
        ),

        tabPanel("Melt", value=2,
                 dataTableOutput("melt_table")
        ),

        tabPanel("pivot Table", value=3,
                 verbatimTextOutput("pivotRefresh"),
                 rpivotTableOutput("mypivot")
        ),

        # tabPanel("Transpose", value=4,
        #          tableOutput('transposeTable')
        # ),

        tabPanel("Save", value=6,
                 uiOutput("warningSave"),
                 tableOutput('sortieTable')
        ),

        tabPanel("Options", value=7,
                 strong("Notice"),
                 uiOutput("noticeOptions")
        )

      )
    )
  )
))
